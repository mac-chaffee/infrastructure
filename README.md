# Infrastructure

The purpose of this project is to run all of my personal projects
in a single kubernetes cluster.

Choosing this infrastucture means I can lean about
container orchestration and have an extensible, scalable
platform on which to build more projects.

## Personal projects running on this new infrastructure

- [ ] meps-metrics: <meps.macchaffee.com>
- [ ] crowdsort: <crowdsort.macchaffee.com>

## Development environment setup

1. Install and start microk8s
    ```bash
    sudo snap install microk8s --classic
    sudo microk8s.start
    microk8s.enable dashboard dns storage ingress
    ```
2. Install k9s: https://github.com/derailed/k9s/releases
3. Install Helm 3 ([source](https://helm.sh/docs/intro/install/#from-the-binary-releases))
    ```
    curl -L https://get.helm.sh/helm-v3.2.1-linux-amd64.tar.gz -o helm.tar.gz
    tar -zxvf helm.tar.gz
    sudo mv linux-amd64/helm /usr/local/bin/helm
    rm -rf helm.tar.gz linux-amd64/
    ```

## Deployment

1. Provision a kubernetes cluster from Digital Ocean (or use [homelab](https://gitlab.com/mac-chaffee/homelab))
2. Download the config file, put it in `~/.kube/` and call it `config`
3. Install all the helm charts
    ```bash
    helm install meps-metrics charts/meps-metrics
    helm install crowdsort charts/crowdsort
    ```
4. Install the ingress controller and the ingress
    ```bash
    helm repo add traefik https://containous.github.io/traefik-helm-chart
    helm repo update
    helm install traefik traefik/traefik
    kubectl apply -f setup/ingress.yaml
    ```
5. Follow the directions [here](https://docs.cert-manager.io/en/latest/getting-started/install/kubernetes.html#steps)
   to set up cert-manager for managing TLS certificates
6. If not using a cloud prodiver, we need to install our own LoadBalancer: https://metallb.universe.tf/

### Deploying upgrades

1. Get the name of the last releases
   ```bash
   helm list
   ```
2. Upgrade the release
   ```bash
   helm upgrade <release> charts/meps-metrics/
   ```

## Other helpful info

### Kubernetifying an existing application

1. Create docker images for all services needed
2. In the same repo, run `helm create (projname)`
3. Modify the yaml files in the generated directory
    1. Delete unused values and delete ingress.yaml (ingress will be handled by the infra repo)
    2. If the application needs to receive traffic, ensure you have at least one Service of type ClusterIP,
       then update the infra repo to point to the new Service
    3. If the application depends on other helm charts, mention them in `requirements.yaml` and run
       `helm dependency update`
4. Run `helm install --dry-run --debug (repo)/helm-charts/` to ensure the templates compile
5. Run `helm install (repo)/helm-charts/` to install for real
6. To update the app later, run `helm upgrade (repo)/helm-charts/`
   * Or `helm delete $(helm list -q)` followed by `helm install`


### Managing helm dependencies

To install a new dependency, follow the directions [here](https://docs.bitnami.com/kubernetes/how-to/create-your-first-helm-chart/#dependencies)
